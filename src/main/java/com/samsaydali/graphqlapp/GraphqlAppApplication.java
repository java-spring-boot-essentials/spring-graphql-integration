package com.samsaydali.graphqlapp;

import com.samsaydali.graphqlapp.config.GraphqlConfiguration;
import graphql.Scalars;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@Import(GraphqlConfiguration.class)
public class GraphqlAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlAppApplication.class, args);
	}


	/*@Bean
	GraphQLSchema schema() {
		return GraphQLSchema.newSchema()
				.query(GraphQLObjectType.newObject()
						.name("query")
						.field(field -> field
								.name("test")
								.type(Scalars.GraphQLString)
								.dataFetcher(environment -> "response")
						)
						.build())
				.build();
	}*/
}

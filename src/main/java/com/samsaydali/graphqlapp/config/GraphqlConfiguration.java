package com.samsaydali.graphqlapp.config;

import com.samsaydali.graphqlapp.Query;
import com.samsaydali.graphqlapp.products.ProductResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GraphqlConfiguration {
    @Bean
    public ProductResolver productResolver() {
        return new ProductResolver();
    }

    @Bean
    public Query query() {
        return new Query();
    }
}

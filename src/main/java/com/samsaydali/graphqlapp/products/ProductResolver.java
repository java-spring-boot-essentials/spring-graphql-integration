package com.samsaydali.graphqlapp.products;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.samsaydali.graphqlapp.products.models.Product;
import com.samsaydali.graphqlapp.subcategories.models.Subcategory;
import com.samsaydali.graphqlapp.subcategories.repositories.SubcategoriesRepository;

public class ProductResolver implements GraphQLResolver<Product> {
    private SubcategoriesRepository subcategoriesRepository;

    public Subcategory getAuthor(Product product) {
        return subcategoriesRepository.getById(product.subcategoryId);
    }
}
package com.samsaydali.graphqlapp.products.models;

import com.samsaydali.graphqlapp.subcategories.models.Subcategory;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
    public List<ProductTranslation> translations;

    @Column(name = "subcategory_id", insertable = false, updatable = false)
    public Long subcategoryId;

    @ManyToOne
    @JoinColumn(name="subcategory_id")
    public Subcategory subcategory;

    public String getTitle() {
        return translations.get(0).title;
    }

}

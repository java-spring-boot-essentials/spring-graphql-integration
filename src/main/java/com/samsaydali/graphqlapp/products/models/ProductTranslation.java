package com.samsaydali.graphqlapp.products.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "product_translations")
public class ProductTranslation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column
    public String title;

    @ManyToOne
    @JoinColumn(name="product_id")
    @JsonIgnore
    public Product product;
}

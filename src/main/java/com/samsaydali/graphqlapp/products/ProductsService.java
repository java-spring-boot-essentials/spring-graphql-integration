package com.samsaydali.graphqlapp.products;

import com.samsaydali.graphqlapp.products.models.Product;
import com.samsaydali.graphqlapp.products.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductsService {

    @Autowired
    private ProductsRepository productsRepository;

    public List<Product> getProducts() {
        return this.productsRepository.findAll();
    }
}

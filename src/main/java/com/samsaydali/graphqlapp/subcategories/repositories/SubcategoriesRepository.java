package com.samsaydali.graphqlapp.subcategories.repositories;

import com.samsaydali.graphqlapp.subcategories.models.Subcategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubcategoriesRepository extends JpaRepository<Subcategory, Long> {

}

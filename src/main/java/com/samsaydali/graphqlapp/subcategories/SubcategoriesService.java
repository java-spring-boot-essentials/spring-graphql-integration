package com.samsaydali.graphqlapp.subcategories;

import com.samsaydali.graphqlapp.subcategories.models.Subcategory;
import com.samsaydali.graphqlapp.subcategories.repositories.SubcategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SubcategoriesService {

    @Autowired
    private SubcategoriesRepository subcategoriesRepository;

    public List<Subcategory> findAll() {
        return this.subcategoriesRepository.findAll();
    }
}

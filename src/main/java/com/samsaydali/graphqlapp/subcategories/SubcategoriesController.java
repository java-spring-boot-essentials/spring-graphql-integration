package com.samsaydali.graphqlapp.subcategories;

import com.samsaydali.graphqlapp.subcategories.models.Subcategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/subcategories")
public class SubcategoriesController {

    @Autowired
    private SubcategoriesService subcategoriesService;

    @GetMapping
    public List<Subcategory> findAll() {
        return subcategoriesService.findAll();
    }
}

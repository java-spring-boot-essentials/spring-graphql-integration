package com.samsaydali.graphqlapp;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.samsaydali.graphqlapp.products.models.Product;
import com.samsaydali.graphqlapp.products.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class Query implements GraphQLQueryResolver {

    @Autowired
    private ProductsRepository productsRepository;

    public List<Product> getRecentPosts(int count, int offset) {
        return productsRepository.findAll();
    }
}

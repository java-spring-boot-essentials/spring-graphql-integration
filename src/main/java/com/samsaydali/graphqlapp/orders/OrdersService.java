package com.samsaydali.graphqlapp.orders;

import com.samsaydali.graphqlapp.orders.models.Order;
import com.samsaydali.graphqlapp.orders.repositories.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersService {

    @Autowired
    private OrdersRepository repository;

    public List<Order> findAll() {
        return repository.findAll();
    }


}

package com.samsaydali.graphqlapp.orders.repositories;

import com.samsaydali.graphqlapp.orders.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdersRepository extends JpaRepository<Order, Long> {
}

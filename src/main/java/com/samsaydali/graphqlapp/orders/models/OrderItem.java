package com.samsaydali.graphqlapp.orders.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.samsaydali.graphqlapp.products.models.Product;

import javax.persistence.*;

@Entity
@Table(name = "order_items")
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @ManyToOne
    @JsonIgnore
    public Order order;

    @ManyToOne()
    public Product product;
}

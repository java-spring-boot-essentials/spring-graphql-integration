package com.samsaydali.graphqlapp.zones;

import com.samsaydali.graphqlapp.zones.models.Zone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nullable;
import java.util.List;

@RestController
@RequestMapping("/api/zones")
public class ZonesController {

    @Autowired
    private ZonesService zonesService;

    @GetMapping
    public List<Zone> findAll(@RequestParam @Nullable String name) {
        return zonesService.findAll(name);
    }
}

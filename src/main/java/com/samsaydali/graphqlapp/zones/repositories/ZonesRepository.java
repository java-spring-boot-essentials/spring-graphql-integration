package com.samsaydali.graphqlapp.zones.repositories;

import com.samsaydali.graphqlapp.zones.models.Zone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZonesRepository extends JpaRepository<Zone, Long> {
}

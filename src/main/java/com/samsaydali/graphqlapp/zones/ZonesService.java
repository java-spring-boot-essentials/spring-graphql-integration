package com.samsaydali.graphqlapp.zones;

import com.samsaydali.graphqlapp.zones.models.Zone;
import com.samsaydali.graphqlapp.zones.repositories.ZonesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZonesService {

    @Autowired
    private ZonesRepository zonesRepository;


    public List<Zone> findAll() {
        return this.zonesRepository.findAll();
    }

    public List<Zone> findAll(String name) {
        return findAll();
    }
}
